package saluta.salutapanicandreporter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Checks incoming SMS for keywords and compares number to existing app contacts
 */

public class IncomingSms extends BroadcastReceiver {
    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent) {
        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String message = currentMessage.getDisplayMessageBody();

                    if (message.equals("Safe") || message.equals("SAFE")) {

                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                        String senderNum = phoneNumber;

                        Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);


                        // Show alert
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(context, senderNum + " is safe.", duration);
                        toast.show();
                    } else {
                        continue;
                    }

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
}
