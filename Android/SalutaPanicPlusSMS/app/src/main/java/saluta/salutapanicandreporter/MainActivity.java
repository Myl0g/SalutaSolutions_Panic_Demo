package saluta.salutapanicandreporter;

import android.Manifest;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.support.v4.app.ActivityCompat;
import android.widget.Button;
import android.widget.EditText;
import java.util.*;

public class MainActivity extends AppCompatActivity {
    //ArrayList<Student> students;

    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Button panicButton = (Button) findViewById(R.id.panicButton);

        panicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage("5617776339", null, "RED ALERT. Reply SAFE when safe", null, null);
                boolean extraStudent = true;
                String[] studentInfo = new String[2];

                try { studentInfo = student.getAllInfo(); }
                catch (Exception e) { extraStudent = false; }

                if (extraStudent == true) {
                    smsManager.sendTextMessage(studentInfo[1], null, "RED ALERT. Reply SAFE when safe", null, null);
                }
/*
                for (int i = 0; i < students.size(); i++) {
                    String name = students.get(i).getName();
                    String phone = students.get(i).getNum();

                    smsManager.sendTextMessage(phone, null, "RED ALERT. Reply SAFE when safe", null, null);
                }
*/

                startActivity(new Intent(getApplicationContext(), Map.class));
            }
        });

        Button contactButton = (Button) findViewById(R.id.contactButton);

        final EditText txt = new EditText(this);
        final String[] m_Text = new String[1];
        final AlertDialog.Builder adb = new AlertDialog.Builder(this)
                .setTitle("Student Name and Number")
                .setMessage("[Name], [Phone Number]")
                .setView(txt)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        m_Text[0] = txt.getText().toString();
                        student = new Student(m_Text[0]);
                    }
                });

        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adb.show();
            }
        });
    }
}
